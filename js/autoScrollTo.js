var scrollY = 0;
var distance = 40;
var speed = 24;
function autoScrollTo(el) {
	var currentY = window.pageYOffset;
	var targetY = document.getElementById(el).offsetTop;
	var bodyHeight = document.body.offsetHeight;
	var yPos = currentY + window.innerHeight;
	var animator = setTimeout('autoScrollTo(\''+el+'\')',24);
	if(yPos > bodyHeight){
		clearTimeout(animator);
	} else {
		if(currentY < targetY-distance){
		    scrollY = currentY+distance;
		    window.scroll(0, scrollY);
	    } else {
		    clearTimeout(animator);
	    }
	}
}
function resetScroller(el){
	var currentY = window.pageYOffset;
    var targetY = document.getElementById(el).offsetTop;
	var animator = setTimeout('resetScroller(\''+el+'\')',speed);
	if(currentY > targetY){
		scrollY = currentY-distance;
		window.scroll(0, scrollY);
	} else {
		clearTimeout(animator);
	}
}



//<!--scroll to top button-->

    $(document).ready(function(){ 
     $(window).scroll(function(){ 
      if ($(this).scrollTop() > 100) { 
       $('#scroll').fadeIn(); 
      } else { 
       $('#scroll').fadeOut(); 
      } 
     }); 
     $('#scroll').click(function(){ 
      $("html, body").animate({ scrollTop: 0 }, 600); 
      return false; 
     }); 
    });
   
  // <!--End of scroll to top button-->

  //droplist menu
  $(document).ready(function(){
        $('.menu-toggle').click(function(){
          $('nav').toggleClass('active')
        })
      })
  //end of drop list


