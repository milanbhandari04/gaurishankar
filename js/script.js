
<script type="text/javascript">
  $(document).ready(function() {
  
  var scrollLink = $('.menu');
  
  // Smooth scrolling
  scrollLink.click(function(e) {
    e.preventDefault();
    $('body,html').animate({
      scrollTop: $(this.hash).offset().top
    }, 1000 );
  });

  $(window).scroll(function(){
  	if ($(this).scrollTop()>5) {
  		$(".gs-sticky-nav").addClass("fixed-top");
  	}
  	else{
  		$("gs-sticky-nav").removeClass("fixed-top");
  	}
  })